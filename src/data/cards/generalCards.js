import { ROOMS } from "constants/roomConstants";
import { TYPES } from "constants/gameConstants";

export const generalCards = [
    {
        id: 1,
        name: "Nouvelle maison",
        type: TYPES.general,
        description: "Ajoutez une nouvelle maison à votre école",
        onActivate: ({ createHouse }) => {
            createHouse()
        }
    },
    {
        id: 2,
        name: "Nouvelle maison",
        type: TYPES.general,
        description: "Ajoutez une nouvelle maison à votre école",
        onActivate: ({ createHouse }) => {
            createHouse()
        }
    },
    {
        id: 3,
        name: "Nouvelle maison",
        type: TYPES.general,
        description: "Ajoutez une nouvelle maison à votre école",
        onActivate: ({ createHouse }) => {
            createHouse()
        }
    },
    {
        id: 4,
        name: "Réféctoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.refectory)
        }
    }, {
        id: 5,
        name: "Réféctoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.refectory)
        }
    }, {
        id: 6,
        name: "Réféctoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.refectory)
        }
    }, {
        id: 7,
        name: "Réféctoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.refectory)
        }
    }, {
        id: 8,
        name: "Réféctoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.refectory)
        }
    }, {
        id: 9,
        name: "Réféctoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.refectory)
        }
    },
    {
        id: 10,
        name: "Dortoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.dormitory)
        }
    },
    {
        id: 11,
        name: "Dortoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.dormitory)
        }
    },
    {
        id: 12,
        name: "Dortoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.dormitory)
        }
    }, {
        id: 13,
        name: "Dortoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.dormitory)
        }
    }, {
        id: 14,
        name: "Dortoire",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.dormitory)
        }
    },
    {
        id: 15,
        name: "Salle des arcanes",
        type: TYPES.general,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.arcaneRoom)
        }
    },
    {
        id: 16,
        name: "Salle des arcanes",
        type: TYPES.arcane,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.arcaneRoom)
        }
    },
    {
        id: 17,
        name: "Salle des arcanes",
        type: TYPES.arcane,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.arcaneRoom)
        }
    },
    {
        id: 18,
        name: "Salle des arcanes",
        type: TYPES.arcane,
        description: "Ajoutez une nouvelle pièce à votre école",
        onActivate: ({ addRoom }) => {
            addRoom(ROOMS.arcaneRoom)
        }
    }

]