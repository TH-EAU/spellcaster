import { Box, Heading, HStack } from "@chakra-ui/react";
import { TYPE_ICONS } from "constants/gameConstants";
import { motion } from "framer-motion"


export default function Card({ type, name, onClick }) {
    return (
        <motion.div
            onClick={onClick}
            whileHover={{ scale: [null, 1.2, 1.15] }}
            transition={{ duration: 0.3 }}
            exit={{ opacity: 0 }}

        >
            <Box
                onClick={onClick}
                cursor="pointer"
                bg="currentcolor"
                w="150px"
                h="300"
                boxShadow="lg"
                rounded="md"
                border="5px solid"
                borderColor="gray.200"
                position="relative"
                overflow="hidden"
                _before={{
                    position: "absolute",
                    content: '""',
                    bg: "blue.100",
                    w: "100px",
                    h: "50px",
                    filter: "blur(60px)",
                    opacity: 0.3
                }}
                _after={{
                    position: "absolute",
                    content: '""',
                    bg: "purple.600",
                    w: "100px",
                    h: "50px",
                    bottom: "0px",
                    right: "0px",
                    filter: "blur(60px)"
                }}
            >
                <Box position="absolute" p={2} >
                    {TYPE_ICONS[type]}
                </Box>
                <HStack>
                    <Heading color="white" size="sm" p={10} pt={0} marginTop={2} >
                        {name}
                    </Heading>
                </HStack>
            </Box>
        </motion.div>

    )
}