import { extendTheme } from '@chakra-ui/react'
import "@fontsource/griffy"


const fonts = {
    heading: "Griffy, cursive",
    body: "Griffy, cursive",
    text: "Griffy, cursive",
}

const styles = {
    global: (props) => ({})
}

const colors = {
    brand: { 100: "#4224C2", 200: "#5500FF" },
}

const theme = extendTheme({ fonts, styles, colors })
export default theme