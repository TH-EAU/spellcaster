export const generalAppointements = [
    {
        name: "Lune de sang",
        protagonist: "un vieux fou",
        image: "",
        message: "La lune était rouge la nuit dernière. C'est un signe ! Le clan est prêt à vous combattre !",
        expire: 30,
        reponses: [
            {
                message: "Oh no...",
                conditions: [],
                consequences: []
            },
            {
                message: "Mais nous sommes en paix!",
                conditions: [],
                consequences: []
            },
            {
                message: "Vous ne pouvez pas faire ça! Nous sommes amis",
                conditions: [],
                consequences: []
            },
            {
                message: "Et pour un petit peu d'or vous oubliez la couleur de la lune?",
                conditions: [],
                consequences: []
            }
        ]
    }
]