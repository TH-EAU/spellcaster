import { generalAppointements } from "data/appointements/generalAppointements";
import useInterval from "hooks/useInterval";
import { createContext, useContext, useEffect } from "react";
import uuid from "react-uuid";
import { getRandomAppointement } from "utils/getRandomCard";
import { getRandomArbitrary } from "utils/math";
import { GameContext } from "./GameContext";

export const WorldContext = createContext(null)

const WorldProvider = ({ children }) => {

    const { addAppointement, days, pause } = useContext(GameContext)

    // useInterval(() => {
    //     if (!pause) {
    //         const app = getRandomAppointement([...generalAppointements])
    //         console.log(app)
    //         addAppointement({
    //             ...app,
    //             date: app.expire + days,
    //             id: uuid()
    //         })
    //     }
    // }, getRandomArbitrary(200, 9000))



    return (
        <WorldContext.Provider value={"couco"} >
            {children}
        </WorldContext.Provider>
    )
}

export default WorldProvider