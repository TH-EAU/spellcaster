import cardGen from "./plop/generator/card.generator.js"
export default function (plop) {
    plop.setGenerator(
        "card", cardGen
    );
};
