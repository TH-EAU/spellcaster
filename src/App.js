import { Box } from "@chakra-ui/react";
import GUI from "components/gui";
import AcceptStudent from "components/modals/accept-student";
import CreateHouse from "components/modals/create-house";
import GetCards from "components/modals/get-cards";
import Terrain from "components/terrain";


function App() {


  return (
    <div className="App">
      <Box h="100vh" w="100vw" >
        <GUI />
        <Terrain />
        <CreateHouse />
        <AcceptStudent />
      </Box>
    </div>
  );
}

export default App;
