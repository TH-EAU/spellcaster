import { useContext } from 'react'
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    Button,
    Input,
} from '@chakra-ui/react'
import { GameContext } from 'contexts/GameContext'
import { useForm } from "react-hook-form";

export default function CreateHouse() {
    const { register, handleSubmit, formState: { errors } } = useForm();


    const { displayCreateHouse, addHouse, setDisplayCreateHouse } = useContext(GameContext)

    const onSubmit = data => {
        addHouse({
            name: data.name,
            specialization: "ta mere"
        })
        onClose()

    }

    const onClose = () => {
        setDisplayCreateHouse(false)
    }

    return (
        <Modal isOpen={displayCreateHouse} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Créer une nouvelle maison</ModalHeader>
                <ModalBody>
                    <form id="create-house-form" onSubmit={handleSubmit(onSubmit)} >
                        <Input  {...register("name", { required: true })} />
                    </form>
                </ModalBody>

                <ModalFooter>
                    <Button variant='ghost' type="submit" form="create-house-form">Valider</Button>
                </ModalFooter>
            </ModalContent>
        </Modal >
    )
}