import { createContext, useEffect, useState } from "react";
import useInterval from "hooks/useInterval";
import uuid from "react-uuid";

import { GAME_SPEED, GENERAL_HOUSE_INITIAL } from "constants/gameConstants";
import { PARAMETERS } from "constants/schoolParameters";
import { cards } from "data/cards";

import { haveADormitory, haveARefectory } from "utils/haveARoom";

export const GameContext = createContext(null)

const GameProvider = ({ children }) => {
    // general
    const [days, setDays] = useState(0)
    const [appointements, setAppointements] = useState([])
    const [gameSpeed, setGameSpeed] = useState(GAME_SPEED.NORMAL)
    const [pause, setPause] = useState(true)
    const [init, setInit] = useState(false)

    // resources
    const [coins, setCoins] = useState(0)
    const [naturalScore, setNaturalScore] = useState(0)
    const [luminousScore, setLuminousScore] = useState(0)
    const [destructionScore, setDestructionScore] = useState(0)
    const [arcanceScore, setArcanceScore] = useState(0)
    const [alchemyScore, setAlchemyScore] = useState(0)

    // in Game
    const [deck, setDeck] = useState([...cards])
    const [hand, setHand] = useState([])
    const [rooms, setRooms] = useState([])
    const [students, setStudents] = useState([])
    const [houses, setHouses] = useState([GENERAL_HOUSE_INITIAL])
    const [admissionFees, setAdmissionFees] = useState(PARAMETERS.ADMISSION_FEES)

    // modals
    const [displayCreateHouse, setDisplayCreateHouse] = useState(false)
    const [displayAcceptStudent, setDisplayAcceptStudent] = useState(false)
    const [displayGetCards, setDisplayGetCards] = useState(false)


    // basic game engine --- general interactions
    useInterval(() => {
        if (!pause) {
            setDays(days + 1)
            updateFinancial()
        }
    }, gameSpeed)

    const addAppointement = (appointement) => {
        // const newAppointement = {
        //     type: "test",
        //     id: uuid(),
        //     date: days + 30
        // }
        setAppointements([appointement, ...appointements])
    }

    const removeAppointement = ({ id }) => {
        const news = appointements.filter((app) => app.id !== id)
        setAppointements(news)
    }

    // resources interactions
    const updateFinancial = () => {
        let addToFinancial = 0;
        addToFinancial += haveADormitory(rooms) ? students.length * PARAMETERS.DORMITORY_FEE : 0
        addToFinancial += haveARefectory(rooms) ? students.length * PARAMETERS.REFECTORY_FEE : 0
        setCoins(coins + addToFinancial)
    }

    // game interraction
    const addCoins = amount => setCoins(coins + amount)

    const addRoom = (room) => {
        setRooms([...rooms, room])
    }

    const addCard = card => {
        setDeck(deck.filter((item) => item.id !== card.id))
        setHand([...hand, card])
    }

    const destroyCard = ({ id }) => {
        setHand(hand.filter((card) => card.id !== id))
    }

    const createHouse = () => {
        setDisplayCreateHouse(true)
    }

    const addHouse = ({ name, specializations }) => {
        setHouses([...houses,
        {
            id: uuid(),
            name,
            specializations
        }])
    }

    const addStudent = ({ name, nature, house }) => {
        addCoins(admissionFees)
        setStudents([...students,
        {
            id: uuid(),
            name,
            nature,
            house
        }])
    }

    useEffect(() => {
        setInit(true)
    }, [])

    return (
        <GameContext.Provider
            value={{
                init,
                setInit,
                rooms,
                addRoom,
                coins,
                addCoins,
                days,
                gameSpeed,
                setGameSpeed,
                pause,
                setPause,
                appointements,
                addAppointement,
                removeAppointement,
                deck,
                setDeck,
                hand,
                setHand,
                addCard,
                destroyCard,
                displayCreateHouse,
                setDisplayCreateHouse,
                createHouse,
                houses,
                addHouse,
                displayAcceptStudent,
                setDisplayAcceptStudent,
                addStudent,
                displayGetCards,
                setDisplayGetCards
            }} >
            {children}
        </GameContext.Provider>
    )
}

export default GameProvider