import { Icon } from '@chakra-ui/react'
import { GiCrownCoin, GiDrinkMe, GiBurningEye, GiCurledLeaf, GiCrystalBars, GiCrucifix } from 'react-icons/gi'

export const APPOINTEMENT_WAITING = 30

export const GAME_SPEED = {
    NORMAL: 4000,
    FAST: 2000,
    SUPER_FAST: 700
}

export const GENERAL_HOUSE_INITIAL = {
    id: "general",
    name: "general"
}

export const TYPES = {
    general: "general",
    natural: "natural",
    luminous: "luminous",
    destruction: "destruction",
    arcane: "arcane",
    alchemy: "alchemy"
}

export const TYPE_ICONS = {
    general: <Icon color="gray.300" as={GiCrownCoin} />,
    natural: <Icon color="green.600" as={GiCurledLeaf} />,
    luminous: <Icon color="yellow.300" as={GiCrucifix} />,
    destruction: <Icon color="red.600" as={GiBurningEye} />,
    arcane: <Icon color="purple.600" as={GiCrystalBars} />,
    alchemy: <Icon color="blue.600" as={GiDrinkMe} />,
}
