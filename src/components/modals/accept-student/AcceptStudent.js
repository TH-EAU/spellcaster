import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    useDisclosure,
    Button,
    Text,
    VStack,
} from '@chakra-ui/react'
import { GameContext } from 'contexts/GameContext'
import { useContext } from 'react'

export default function CreateHouse() {
    const { onClose } = useDisclosure()
    const { displayAcceptStudent, setDisplayAcceptStudent, houses, addStudent } = useContext(GameContext)

    const onSubmit = (e) => {
        addStudent({
            name: "Didier Larose",
            nature: "Connard insignifiant",
            house: e.target.value
        })
        setDisplayAcceptStudent(false)
    }



    return (
        <Modal isOpen={displayAcceptStudent} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Voici un élève</ModalHeader>
                <ModalBody>
                    <VStack>
                        {houses.map((house) => {
                            return <Button value={house.id} onClick={onSubmit}>{house.name}</Button>
                        })}
                    </VStack>
                </ModalBody>

                <ModalFooter>
                    {/* <Button variant='ghost'>Valider</Button> */}
                </ModalFooter>
            </ModalContent>
        </Modal>
    )
}