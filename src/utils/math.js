export function percentage(partialValue, totalValue) {
    return (100 * partialValue) / totalValue;
}

export function getRandomArbitrary(min, max) {
    return Math.random() * (max - min) + min;
}