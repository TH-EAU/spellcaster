import { Box, HStack } from "@chakra-ui/react";
import EnablelableCardWrapper from "components/enablelable-card-wrapper";
import { GameContext } from "contexts/GameContext";
import { Reorder } from "framer-motion";
import { useContext } from "react";

export default function Playmat() {
    const { hand, setHand } = useContext(GameContext)

    return (
        <Box bg="red.700" >
            <Reorder.Group as="div"
                axis="x" values={hand} onReorder={setHand}>
                <HStack>
                    {hand.map((card) => (
                        <Reorder.Item as="div" key={card.id} value={card}
                            initial={{ opacity: 0, y: 30 }}
                            animate={{
                                opacity: 1,
                                y: 0,
                                transition: { duration: 0.15 }
                            }}
                            exit={{ opacity: 0 }}
                        >
                            <EnablelableCardWrapper {...card} />
                        </Reorder.Item>
                    ))}
                </HStack>
            </Reorder.Group>
        </Box>
    )
}