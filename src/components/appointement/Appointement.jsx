import { useContext, useEffect, useState } from "react";
import {
    Box,
    Button,
    useDisclosure,
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react";
import { GameContext } from "contexts/GameContext";
import { CircularProgress, CircularProgressLabel } from '@chakra-ui/react'
import { percentage } from "utils/math";
import { APPOINTEMENT_WAITING } from "constants/gameConstants";


export default function Appointement({ id, message, date }) {
    const { setPause, removeAppointement, days } = useContext(GameContext)
    const remainingTime = date - days
    const percent = 100 - (percentage(remainingTime, APPOINTEMENT_WAITING))

    const { isOpen, onOpen, onClose } = useDisclosure()



    const passEvent = () => {
        onClose()
        setPause(false)
        removeAppointement({ id })
    }

    const triggerEvent = () => {
        onOpen()
        setPause(true)
    }

    useEffect(() => {
        if (date === days) {
            onOpen()
            setPause(true)
        }
    }, [date, days])



    return (isOpen ? (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader>Modal Title</ModalHeader>
                <ModalCloseButton />
                <ModalBody>
                    {message}
                </ModalBody>

                <ModalFooter>

                    <Button variant='ghost' onClick={passEvent} >Ok</Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    ) : (
        <Box  >
            <CircularProgress onClick={triggerEvent} size='80px' thickness='4px' color="red" value={percent} />
        </Box>
    )
    )

}