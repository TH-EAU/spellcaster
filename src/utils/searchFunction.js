export const searchById = (arr, id) => {
    return arr.filter((elem) => elem.id === id)
}

export const searchByName = (arr, name) => {
    return arr.filter((elem) => elem.name === name)
}