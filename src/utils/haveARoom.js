import { ROOMS } from "constants/roomConstants"

export const haveARefectory = (rooms) => {
    return rooms.filter((elem) => elem.name === ROOMS.refectory.name).length > 0
}

export const haveADormitory = (rooms) => {
    return rooms.filter((elem) => elem.name === ROOMS.dormitory.name).length > 0
}