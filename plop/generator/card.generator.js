const cards = require("../../src/data/cards.js")
const rooms = require("../../src/constants/roomConstants");


const description = "generate a new card";

const prompts = [
  {
    type: "input",
    name: "name",
    message: "Name of the card :",
  },
  {
    type: "list",
    name: "utility",
    message: "Utilité de la carte",
    choices: [
      "Ajout de pièce",
      "Autres"
    ],
  },
  {
    type: "list",
    name: "type",
    message: "Type of the card :",
    choices: [
      "general",
      "arcane",
      "alchemy",
      "luminous",
      "destruction"
    ]
  },
  {
    type: "input",
    name: "desc",
    message: "Description of the card :",
  },
  {
    type: "list",
    name: "activation",
    message: "OnActivate method :",
    choices: [
      "addRoom",
      "createHouse"
    ]
  },
  {
    type: "list",
    name: "parameters",
    message: "OnActivate parameter :",
    choices: Object.keys(rooms.ROOMS)
  }
];

const actions = (data) => {
  data.param = data.utility === "Ajout de pièce" ? 'ROOMS.' + data.parameters : ""

  return [{
    type: "append",
    pattern: /(\/\/ INSERT CARD)/g,
    path: "src/data/cards.js",
    templateFile: "plop/templates/card.js.hsb",
    data: {
      id: cards.cards.length
    }
  }];
}

module.exports = {
  description,
  prompts,
  actions,
};