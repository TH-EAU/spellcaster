import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalFooter,
    ModalBody,
    ModalCloseButton,
    Button,
    useDisclosure,
    Box,
    Icon,
    HStack,
    Progress,
    VStack
} from '@chakra-ui/react'
import { generalCards } from 'data/cards/generalCards'
import Card from 'components/card'
import { GameContext } from 'contexts/GameContext'
import { useContext, useEffect, useState } from 'react'
import getRandomCard from 'utils/getRandomCard'

import { GiCardPlay } from "react-icons/gi"

export default function GetCards() {
    const { isOpen, onOpen, onClose } = useDisclosure()
    const [pioche, setPioche] = useState([])
    const { addCard, deck, init, setInit, setPause, coins, addCoins } = useContext(GameContext)

    useEffect(() => {
        if (isOpen) {
            setPause(true)
            if (init) {
                setPioche([
                    deck[0],
                    deck[1],
                    deck[6]
                ])
            } else {
                setPioche([
                    getRandomCard(deck),
                    getRandomCard(deck),
                    getRandomCard(deck)
                ])
            }
        }
    }, [isOpen])

    const handleChoice = (card) => {
        !init && addCoins(-400)

        addCard(card)
        if (init) {
            setPioche(pioche.filter((elem) => elem !== card))
            if (pioche.length <= 1) {
                setInit(false)
                setPause(false)
                onClose()
            }
            setPause(false)
            return;
        }
        setPause(false)
        onClose()
    }

    return (
        <>


            <Button onClick={onOpen} leftIcon={<Icon as={GiCardPlay} />} disabled={!init && coins < 400} >piocher</Button>



            <Modal isOpen={isOpen} size="xl" >
                <ModalOverlay />
                <ModalContent bg="transparent" boxShadow="none" >
                    <ModalBody  >

                        <HStack gap={4} >
                            {pioche.map((card) => <Card onClick={() => handleChoice(card)} {...card} />)}
                        </HStack>
                    </ModalBody>
                </ModalContent>
            </Modal>
        </>
    )
}