import { useContext } from "react"
import { Box, Button, Grid, GridItem, HStack, Progress, VStack } from "@chakra-ui/react"
import Appointement from "components/appointement"
import { GAME_SPEED } from "constants/gameConstants"
import { GameContext } from "contexts/GameContext"
import { Reorder } from "framer-motion"

import { Icon } from '@chakra-ui/react'
import { GiCrownCoin } from 'react-icons/gi'
import Card from "components/card"
import getRandomCard, { getRandomAppointement } from "utils/getRandomCard"
import uuid from "react-uuid"
import GetCards from "components/modals/get-cards"
import Playmat from "components/playmat"
import { generalAppointements } from "data/appointements/generalAppointements"
import { percentage } from "utils/math"

export default function GUI() {
    const { coins, days, setGameSpeed, setPause, pause, appointements, addAppointement, setDisplayGetCards, setDisplayAcceptStudent, addCard, hand, setHand, deck, addStudent } = useContext(GameContext)

    const changeGameSpeed = (e) => {
        if (pause) {
            setPause(false)
        }
        setGameSpeed(e.target.value)
    }

    const handlePause = () => {
        setPause(true)
    }

    const nexAPp = () => {
        const app = getRandomAppointement([...generalAppointements])
        addAppointement({
            ...app,
            date: app.expire + days,
            id: uuid()
        })
    }

    // const getCard = () => {
    //     const choosenCard = getRandomCard(deck)
    //     choosenCard && addCard(getRandomCard(deck))
    // }

    const getStudent = () => {
        setDisplayAcceptStudent(true)
    }


    return (
        <Grid templateColumns='repeat(1, 2fr)' templateRows='repeat(12, 1fr)' h="full" position="absolute" w="full" >
            <GridItem rowSpan={2}>
                <Button onClick={nexAPp}>add event</Button>

                <Button onClick={getStudent}>add student</Button>
            </GridItem>
            <GridItem rowSpan={8} position="relative" >
                <VStack position="absolute" left="0" bottom="0">
                    {appointements.map((appoint, indx) => <Appointement {...appoint} />)}
                </VStack>
            </GridItem>
            <GridItem rowSpan={2} >
                <Grid templateColumns='repeat(7, 2fr)' h={0} w="full" >
                    <GridItem colSpan={1}>trucs la </GridItem>
                    <GridItem colSpan={4}  >
                        <Playmat />
                    </GridItem>
                    <GridItem colSpan={2}>
                        <VStack>
                            <Box bg="gray.800" p={2} rounded="2xl" color="white" >
                                <Icon as={GiCrownCoin} color="yellow" /> {coins}
                            </Box>
                            <GetCards />
                            <Progress value={percentage(coins, 400)} w="70px" h="5px" />
                            <Box>
                                Days : {days}
                                <HStack>
                                    <Button onClick={handlePause} >||</Button>
                                    <Button value={GAME_SPEED.NORMAL} onClick={changeGameSpeed} >{`>`}</Button>
                                    <Button value={GAME_SPEED.FAST} onClick={changeGameSpeed}>{`>>`}</Button>
                                    <Button value={GAME_SPEED.SUPER_FAST} onClick={changeGameSpeed}>{`>>>`}</Button>
                                </HStack>
                            </Box>
                        </VStack>
                    </GridItem>
                </Grid>
            </GridItem>
        </Grid>
    )
}