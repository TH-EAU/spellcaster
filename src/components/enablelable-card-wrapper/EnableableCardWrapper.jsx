import Card from "components/card";
import { Box, Button, Heading, HStack, Icon } from "@chakra-ui/react";
import { TYPE_ICONS } from "constants/gameConstants";
import { GameContext } from "contexts/GameContext";
import { motion, useDragControls } from "framer-motion"
import { useContext, useState } from "react";

export default function EnableableCardWrapper({ id, onActivate, ...rest }) {
    const actions = useContext(GameContext)
    const controls = useDragControls()

    const activeEffect = (event, info) => {
        console.log(info.point)
        if (info.point.y < 600) {

            onActivate(actions)
            actions.destroyCard({ id })
        }
    }





    return (
        <motion.div
            drag="y"
            dragConstraints={{ left: 0, right: 0, top: 0, bottom: 0 }}
            dragElastic={1}
            dragTransition={{ bounceStiffness: 600, bounceDamping: 40 }}
            dragControls={controls}
            onDragEnd={activeEffect}
            onDrag={(event, info) => console.log(info.point.x, info.point.y)}
        >
            <Card {...rest} />
        </motion.div>
    )
}