const ROOMS = {
    refectory: {
        name: "Refectoire",
        onEnter: () => {
            console.log("un personnage est rentré")
        }
    },
    dormitory: {
        name: "Dortoir",
        onEnter: () => {
            console.log("un personnage est rentré")
        }
    },
    arcaneRoom: {
        name: "Salle des arcances",
        onEnter: () => {
            console.log("un personnage est rentré")
        }
    }
}

module.exports = {
    ROOMS
}